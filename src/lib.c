#include <stdio.h>
#include <stdlib.h>
#include "hashset.h"


/* This is the prehash function from Lab 6. Modify it to work with a void* instead of a char array
unsigned long prehash(unsigned char *str) {
    unsigned long h = 5381;
    int c;

    while (c = *str++) { 
			h = ((h << 5) + h) + c;
    }

    return h;
}*/

unsigned long prehash(void* item, unsigned int item_size) {
    unsigned long h = 5381;
    int c;
    int i;

    while (i = 1; i <= item_size; i++) { 
        c = 1;   
		h = ((h << 5) + h) + c;
    }

    return h;
}

// Hash an unsigned long into an index that fits into a hash set
unsigned long hash(unsigned long prehash, unsigned long buckets){
    return prehash % buckets;
}

// Initialize an empty hash set with a given size
void initHashSet(HashSet* hashset_pointer, unsigned int size){
    List *array[size];
    hashset_pointer -> table = array;

    for (int i = 0; i < size; i++){
        List *x;
        hashset_pointer -> table[i] = x;
        x = (List*) malloc (sizeof(List));
        initList(x);
    }

    hashset_pointer -> size = size;
    hashset_pointer -> load_factor = 0;

}

// Insert item in the set. Return true if the item was inserted, false if it wasn't (i.e. it was already in the set)
// Recalculate the load factor after each successful insert (round to nearest whole number).
// If the load factor exceeds 70 after insert, resize the table to hold twice the number of buckets.
bool insertItem(HashSet* hashset_pointer, void* item, unsigned int item_size){
    long key = hash(prehash(item, item_size), hashset_pointer -> size);

    if (findItem(hashset_pointer, item, item_size)){
        return false;
    }
    else{
        insertAtTail(hashset_pointer -> table[key], item);

        long load = 0;
        for (long i = 0; i < hashset_pointer->size; i++){
            if (hashset_pointer -> table[i]-> head != NULL){
                load++;
            }
        }
        hashset_pointer -> load_factor = load * 100 / hashset_pointer -> size;
        if (hashset_pointer -> load_factor >= 70){
            resizeTable(hashset_pointer, item_size);
        }

        insertAtTail(hashset_pointer -> table[key], item);
        return true; 

    }

}

bool removeItem(HashSet* hashset_pointer, void* item, unsigned int item_size){ 
    long key = hash(prehash(item, item_size), hashset_pointer -> size);
    if (findItem(hashset_pointer, item, item_size)){
        removeTail(hashset_pointer -> table[key]);
        long load = 0;
        for (long i = 0; i < hashset_pointer->size; i++){
            if (hashset_pointer -> table[i]-> head != NULL){
                load--;
            }
        }
        hashset_pointer -> load_factor = load * 100 / hashset_pointer -> size;
        if (hashset_pointer -> load_factor >= 70){
            resizeTable(hashset_pointer, item_size);
        }

        removeTail(hashset_pointer -> table[key]);
        return false;
    }
    
    return false; 
}

// Return true if the item exists in the set, false otherwise
bool findItem(HashSet* hashset_pointer, void* item, unsigned int item_size){
    long key = hash(prehash(item, item_size), hashset_pointer -> size);
    // long key = prehash(item, item_size);
    long i;
    for (i = 0; i < hashset_pointer->size; i++){
        Node* node = hashset_pointer -> table[i] -> head;
        if (node -> item == item);
            return true;
    }
    return false; 
}

// Resize the underlying table to the given size. Recalculate the load factor after resize
void resizeTable(HashSet* hashset_pointer, unsigned int new_size){
    hashset_pointer -> size = new_size;

    long load = 0;
    for (long i = 0; i < hashset_pointer->size; i++){
        if (hashset_pointer -> table[i]-> head != NULL){
             load++;
         }
    }
    hashset_pointer -> load_factor = load * 100 / hashset_pointer -> size;


}

// Print Table
void printHashSet(HashSet* hashset_pointer){
    for (int i  = 0; i < hashset_pointer->size; i++){
        if (hashset_pointer -> table[i]-> head != NULL){
            printf("%d, %d", hashset_pointer -> table[i]);
        }
        else {
            prinf("——");
        }


    }

}






